"use client"
import styles from '@/css/Login.module.css';
import customstyle from '@/css/ForgotPassword.module.css';
import Input from '@/constants/Form/Input';
import Link from 'next/link';
import { useLocale, useTranslations } from 'next-intl';
import React, { useState } from 'react';
import validator from 'validator';
import api from '@/axiosService';
import { useRouter } from 'next/navigation';
import { Image } from 'antd';

const ForgotPasswordPage: React.FC = () => {
    const [email, setEmail] = useState('');
    const locale = useLocale();
    const [isEmailValid, setIsEmailValid] = useState(false);
    const [errorMessage, setErrorMessage] = useState('');
    const router = useRouter();

    const handleSubmit = async () => {
        
        if (!validator.isEmail(email)) {
            setIsEmailValid(false);
            setErrorMessage('Kindly input a valid email address');
            return;
        }
        const postData = {
            email: email,
        }
        try {
            const res = await api.post('auth/forget-password',postData)
            sessionStorage.setItem('token', res.data.messsages.token);
            sessionStorage.setItem('email', email);
            router.push(`/${locale}/resetpassword`);
            console.log(res.data.messsages.token)
        }
        catch (error) {
            console.log(error);
            setIsEmailValid(false);
            setErrorMessage('This email does not exist in our system, kindly register first!');
        } 
    };
    
    const handleEmailChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        setEmail(e.target.value);
        setIsEmailValid(e.target.value !== '');
    };
    const handleCancelButton = () => {
       
        router.push(`/${locale}/login`);
    };

    return (
      <>
        <div className={styles.inputform}>
          <h5 className={customstyle.forgot}>Forgot password?</h5>
          <p className={customstyle.label}>
            Kindly input your registered email here, we will send you a new
            password to your mailbox.
          </p>
          <div className={styles.input}>
            <svg
              xmlns="http://www.w3.org/2000/svg"
              width="32"
              height="32"
              viewBox="0 0 32 32"
              fill="none"
              className={styles.icon}
            >
              <path
                d="M29.3333 8.00001C29.3333 6.53334 28.1333 5.33334 26.6667 5.33334H5.33334C3.86667 5.33334 2.66667 6.53334 2.66667 8.00001V24C2.66667 25.4667 3.86667 26.6667 5.33334 26.6667H26.6667C28.1333 26.6667 29.3333 25.4667 29.3333 24V8.00001ZM26.6667 8.00001L16 14.6667L5.33334 8.00001H26.6667ZM26.6667 24H5.33334V10.6667L16 17.3333L26.6667 10.6667V24Z"
                fill="#5D5D5D"
              />
            </svg>
            <Input
              type="text"
              name="username"
              placeholder="Email"
              className={styles.inputsection}
              style={{ marginBottom: "24px" }}
              onChange={handleEmailChange}
              value={email}
            />
            {!isEmailValid && (
              <p className={customstyle.error}>{errorMessage}</p>
            )}
          </div>

          <div className={customstyle.buttonsection}>
            <button
              className={
                isEmailValid ? customstyle.sendbtnActive : customstyle.sendbtn
              }
              disabled={!isEmailValid}
              onClick={handleSubmit}
            >
              SEND
            </button>
            <button
              className={customstyle.cancelbtn}
              onClick={handleCancelButton}
            >
              CANCEL
            </button>
          </div>
          <div className={styles.account}>
            <p>Don't have an account?</p>
            <Link
              href={`/${locale}/manager`}
              className={styles.customlink}
              passHref
            >
              Register
            </Link>
          </div>
        </div>
      </>
    );
};

export default ForgotPasswordPage;
