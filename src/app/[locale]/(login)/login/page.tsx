"use client";
import * as React from "react";
import "bootstrap/dist/css/bootstrap.min.css";
// import  from "@/constants/Form/Input";
import styles from "@/css/Login.module.css";
import { CheckboxChangeEvent } from "antd/es/checkbox/Checkbox";
import Link from "next/link";
import { useState, useEffect } from "react";
import { useSelector } from "react-redux";
import Cookies from "js-cookie";
import { useRouter } from "next/navigation";
import Button from "@/constants/Form/Button";
import Modal from "@/constants/Modal/FirstLogModal";
import { toast } from "react-hot-toast";
import api from "@/axiosService";
import { useLocale, useTranslations } from "next-intl";
import { useAppDispatch } from "@/lib/hooks";
import { setLoading } from "@/lib/features/loadingSlice";
import { initializeUser } from "@/lib/features/user/userSlice";
import { ConfigProvider, Image, Input, Checkbox } from "antd";

const LoginPage: React.FC<{}> = () => {
  const t = useTranslations("Login");
  const locale = useLocale();
  const router = useRouter();
  const dispatch = useAppDispatch();
  const user = useSelector((state: any) => state.user.value);
  const [errorMessage, setErrorMessage] = useState("");
  const [errorMessage1, setErrorMessage1] = useState("");
  const [errorMessage2, setErrorMessage2] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [isNewAccount, setIsNewAccount] = useState(false);
  const [newPassword, setNewPassword] = useState("");
  const [currentPassword, setCurrentPassword] = useState("");
  const [confirmNewPassword, setConfirmNewPassword] = useState("");
  const user_id = user ? (user.id ? user.id : 0) : 0;
  const isFormValid = email !== "" && password !== "";
  const validatePassword = (password: string) => {
    const passwordRegex = /^(?=.*[A-Z])(?=.*\d).{8,}$/;
    return passwordRegex.test(password);
  };
  const handleLogin = async () => {
    const postData = {
      email: email,
      password: password,
    };

    try {
      dispatch(setLoading(true));
      const res = await api.post("auth/login", postData);
      toast.success(t("success"));
      sessionStorage.setItem("current_password", password);
      Cookies.set("token", res.data.data.token);
      Cookies.set("type", res.data.data.user.type);
      const user = res.data.data.user;
      dispatch(initializeUser(user));
      const first_login = user.is_first_login;
      if (first_login === 0) {
        setIsNewAccount(true);
        openModal();
      } else {
        if (user.type === 0) {
          router.push(`/${locale}/company`);
        } else {
          router.push(`/${locale}/room`);
        }
      }
    } catch (error) {
      console.log(error);
      toast.error(t("error"));
    } finally {
      dispatch(setLoading(false));
    }
  };

  const [isModalOpen, setIsModalOpen] = useState(false);

  const openModal = () => {
    setIsModalOpen(true);
  };

  const closeModal = () => {
    setIsModalOpen(false);
  };
  const handleSaveChangePassword = async () => {
    if (!validatePassword(newPassword)) {
      setErrorMessage(
        "New password must be at least 8 characters long, contain at least one uppercase letter, and one digit."
      );
      return;
    }
    if (newPassword !== confirmNewPassword) {
      setErrorMessage1("Passwords are not the same");
      return;
    }
    if (newPassword === currentPassword) {
      setErrorMessage(
        "New password should be different from the current password"
      );
      return;
    }
    try {
      const res = await api.post("auth/user/reset-password", {
        user_id: user_id,
        old_password: sessionStorage.getItem("current_password"),
        new_password: newPassword,
      });
      closeModal();
      if (user.type === 0) {
        router.push(`/${locale}/company`);
      } else {
        router.push(`/${locale}/room`);
      }
      const first_login = user.is_first_login;
      if (first_login) {
        user.is_first_login = 1;
        try {
          const res = await api.post("store-users", {
            user_id: user_id,
            is_first_login: 1,
          });
        } catch (error: any) {}
      }
    } catch (error: any) {
      if (error.response && error.response.status === 400) {
        setErrorMessage2("Please re-enter current password");
      }
    }
  };
  const [passwordVisible, setpasswordVisible] = useState(false);
  const [changepasswordVisible, setchangepasswordVisible] = useState(false);
  const onChange = (e: CheckboxChangeEvent) => {};
  return (
    <>
      <div className={styles.inputform}>
        <ConfigProvider
          theme={{
            token: {
              colorPrimary: "#225560",
            },
            components: {
              Input: {
                hoverBorderColor: "#225560",
                colorPrimary: "#225560",
                activeBorderColor: "#fff",
                algorithm: true,
                fontFamily: "Be Vietnam Pro",
              },
              Checkbox: {
                colorPrimary: "#225560",
                algorithm: true,
              },
            },
          }}
        >
          <div className={styles.input}>
            <Input
              bordered={false}
              prefix={
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  width="32"
                  height="32"
                  viewBox="0 0 32 32"
                  fill="none"
                  style={{ marginRight: "8px" }}
                  // className={styles.icon}
                >
                  <path
                    d="M29.3333 8.00001C29.3333 6.53334 28.1333 5.33334 26.6667 5.33334H5.33334C3.86667 5.33334 2.66667 6.53334 2.66667 8.00001V24C2.66667 25.4667 3.86667 26.6667 5.33334 26.6667H26.6667C28.1333 26.6667 29.3333 25.4667 29.3333 24V8.00001ZM26.6667 8.00001L16 14.6667L5.33334 8.00001H26.6667ZM26.6667 24H5.33334V10.6667L16 17.3333L26.6667 10.6667V24Z"
                    fill="#5D5D5D"
                  />
                </svg>
              }
              type="text"
              name="username"
              placeholder="Email"
              className={styles.inputsection}
              style={{ marginBottom: "24px", gap: 0, borderRadius: 0 }}
              onChange={(e: any) => setEmail(e.target.value)}
              value={email}
            ></Input>{" "}
          </div>
          <div className={styles.input}>
            <Input.Password
              bordered={false}
              prefix={
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  width="32"
                  height="32"
                  viewBox="0 0 32 32"
                  fill="none"
                  style={{ marginRight: "8px" }}
                  // className={styles.icon}
                >
                  <path
                    d="M26 9.5H22.5V7C22.5 5.27609 21.8152 3.62279 20.5962 2.40381C19.3772 1.18482 17.7239 0.5 16 0.5C14.2761 0.5 12.6228 1.18482 11.4038 2.40381C10.1848 3.62279 9.5 5.27609 9.5 7V9.5H6C5.33696 9.5 4.70107 9.76339 4.23223 10.2322C3.76339 10.7011 3.5 11.337 3.5 12V26C3.5 26.663 3.76339 27.2989 4.23223 27.7678C4.70107 28.2366 5.33696 28.5 6 28.5H26C26.663 28.5 27.2989 28.2366 27.7678 27.7678C28.2366 27.2989 28.5 26.663 28.5 26V12C28.5 11.337 28.2366 10.7011 27.7678 10.2322C27.2989 9.76339 26.663 9.5 26 9.5ZM12.5 7C12.5 6.07174 12.8687 5.1815 13.5251 4.52513C14.1815 3.86875 15.0717 3.5 16 3.5C16.9283 3.5 17.8185 3.86875 18.4749 4.52513C19.1313 5.1815 19.5 6.07174 19.5 7V9.5H12.5V7ZM25.5 25.5H6.5V12.5H25.5V25.5ZM18 19C18 19.3956 17.8827 19.7822 17.6629 20.1111C17.4432 20.44 17.1308 20.6964 16.7654 20.8478C16.3999 20.9991 15.9978 21.0387 15.6098 20.9616C15.2219 20.8844 14.8655 20.6939 14.5858 20.4142C14.3061 20.1345 14.1156 19.7781 14.0384 19.3902C13.9613 19.0022 14.0009 18.6001 14.1522 18.2346C14.3036 17.8692 14.56 17.5568 14.8889 17.3371C15.2178 17.1173 15.6044 17 16 17C16.5304 17 17.0391 17.2107 17.4142 17.5858C17.7893 17.9609 18 18.4696 18 19Z"
                    fill="#5D5D5D"
                  />
                </svg>
              }
              // type={passwordVisible ? "text" : "password"}
              name="password"
              placeholder="Password"
              className={styles.inputsection}
              style={{
                gap: 0,
                borderRadius: 0,
                fontFamily: "Be Vietnam Pro",
              }}
              onChange={(e: any) => setPassword(e.target.value)}
              value={password}
            ></Input.Password>{" "}
            <Image
              src={passwordVisible ? "/showpass.svg" : "/hidepass.svg"}
              alt=""
              preview={false}
              className={styles.showhide}
              onClick={() => setpasswordVisible(!passwordVisible)}
            />
          </div>

          <div className={styles.forgot}>
            <Checkbox onChange={onChange}>Remember me</Checkbox>
            <Link
              href={`/${locale}/forgotpassword`}
              className={styles.customlink}
            >
              Forgot password?
            </Link>
          </div>
        </ConfigProvider>
        <Button
          type="button"
          className={styles.loginbtn}
          onClick={handleLogin}
          style={{ backgroundColor: isFormValid ? "#225560" : "#8B8B8B" }}
        >
          LOG IN
        </Button>
        <div className={styles.account}>
          <p>Don't have an account?</p>
          <Link
            href={`/${locale}/manager`}
            className={styles.customlink}
            passHref
          >
            Register
          </Link>
        </div>
        <div>
          {isModalOpen && (
            <Modal
              title="Kindly change your password for first time log in."
              onClose={closeModal}
            >
              {
                <>
                  <div className={styles.inputgroup}>
                    <div className={styles.inputform1}>
                      <Image
                        src="/pass.svg"
                        alt=""
                        className={styles.icon1}
                        preview={false}
                      />
                      <input
                        type={changepasswordVisible ? "text" : "password"}
                        name="password"
                        placeholder="Password"
                        className={styles.inputsection}
                        value={newPassword}
                        onChange={(e) => {
                          setNewPassword(e.target.value);
                          setErrorMessage("");
                        }}
                      />
                      <Image
                        src={
                          changepasswordVisible
                            ? "/showpass.svg"
                            : "/hidepass.svg"
                        }
                        preview={false}
                        alt=""
                        className={styles.showhide2}
                        onClick={() =>
                          setchangepasswordVisible(!changepasswordVisible)
                        }
                      />
                      <p className={styles.error}>{errorMessage}</p>
                    </div>
                    <div className={styles.inputform1}>
                      <Image
                        src="/pass.svg"
                        alt=""
                        className={styles.icon1}
                        preview={false}
                      />
                      <input
                        type={changepasswordVisible ? "text" : "password"}
                        name="password"
                        placeholder="Confirm Password"
                        className={styles.inputsection}
                        value={confirmNewPassword}
                        onChange={(e) => {
                          setConfirmNewPassword(e.target.value);
                          setErrorMessage("");
                        }}
                      />
                      <Image
                        preview={false}
                        src={
                          changepasswordVisible
                            ? "/showpass.svg"
                            : "/hidepass.svg"
                        }
                        alt=""
                        className={styles.showhide2}
                        onClick={() =>
                          setchangepasswordVisible(!changepasswordVisible)
                        }
                      />
                      <p className={styles.error}>{errorMessage1}</p>
                    </div>
                  </div>
                  <div className={styles.btngroup}>
                    <Button
                      className={styles.passbtn}
                      onClick={handleSaveChangePassword}
                    >
                      CHANGE PASSWORD
                    </Button>
                  </div>
                </>
              }
            </Modal>
          )}
        </div>
      </div>
    </>
  );
};
export default LoginPage;
